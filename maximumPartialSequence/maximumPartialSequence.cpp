#include <iostream>
using namespace std;

// Complexity = O(n)
int max_subarray(int* A, int n){
    int best = A[0];
    int s = A[0];
    for(int i = 1; i < n; ++i){
        if(s<0){
            s = 0;
        }
        s += A[i];
        if(s>best){
            best = s;
        }
    }
    return best;
}


int main(){
    int array[7] = {16, 2, -55, 92, 0, -2, 24};
    int ans = max_subarray(array, 7);
    cout << "The 1st maximal partial sequence is: "<<ans<<"\n";
    
    /* Generating a random vector and finding its maximum partial sequence.
    int randomArray[8];
    for(int i=0; i<8; ++i){
        randomArray[i] = rand()%100;
    }
    cout<<"The random array genarated is: ";
    for(int i=0; i<8; ++i){
        cout<< randomArray[i]<<' ';
    }

    int ans2 = max_subarray(randomArray, 8);
    cout << "The 2nd maximal partial sequence is: " << ans2 << ".";
    */

    return 0;
}